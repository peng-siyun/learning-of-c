#include <iostream>
#include <string>
using namespace std;

void test_string()//采用下标访问每一个字符
{
	string s1("hello world");


	for (size_t i = 0; i < s1.size(); ++i)
	{
		cout << s1[i] << " " ;
	}
	cout << endl;
}

void test_string1() //使用迭代器访问每一个字符，所有容器都支持迭代器这种方式访问修改
//因为string类有operator[]运算符重载，所以迭代器效果不佳，其他容器不支持下标加[],所以用迭代器。
{
	string s1("hello world");
	string::iterator it = s1.begin();
	while (it != s1.end())
	{
		cout << *it << " ";
		++it;
	}
	cout << endl;
}

void test_string3() //使用反向迭代器访问每一个字符
{
	string s1("hello world");
	//string::reverse_iterator rit = s1.rbegin();
	auto rit = s1.rbegin();//可以使用auto自动推导
	while (rit != s1.rend())
	{
		cout << *rit << " ";
		++rit;
	}
	cout << endl;
}

void test_string2()//使用范围for访问每一个字符
{
	string s1("hello world");
	for (auto e : s1)
	{
		cout << e << " ";
	}
	cout << endl;
}

void test()
{
	string s;
	size_t sz = s.capacity();//观察string类增容的效果
	cout << "capacity change: " << sz << '\n';
	for (int i = 0; i < 100; ++i)
	{
		s.push_back('c');
		if (sz != s.capacity())
		{
			sz = s.capacity();
			cout << "capacity change: " << sz << '\n';
		}
	}
}
int main()
{
	////存的不是字母，而是字母对应的ASCII码表
	//char str1[] = "hello";//ASCII编码表：表示英文的编码表
	//char str2[] = "吃饭";//unicode：表示全世界文字的编码表 utf-8 utf-16，一个汉字两个字节

	//str2[1] += 1;
	//str2[3] += 1;

	//str2[1] += 1;
	//str2[3] += 1;

	//cout << sizeof(char) << endl;
	//cout << sizeof(wchar_t) << endl;//两个字节

	//test_string();
	//test_string1();
	//test_string2();
	//test_string3();

	string s1("hell");

	s1 += 'o';//尾插可以使用+=
	s1.push_back(' ');
	s1 += "world";
	cout << s1 << endl;//对流插入运算符<<进行重载
	cout << s1.c_str() << endl;//c_str()识别的char*，返回他的地址；按字符串打印


	//cout << s1 << endl;
	test();

	string s3;
	s3.reserve(100);//s.reserve(1000)申请至少能存储1000个数据的空间，减少增容

	string s4;
	s4.resize(100);//开空间并给这些空间初始值初始化，不给值默认给\0


	return 0;
}