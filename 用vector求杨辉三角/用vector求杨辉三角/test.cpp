#include <iostream>
#include <vector>
using namespace std;
#define NUM 5

int main()
{
	vector<vector<int>> vv;
	vv.resize(NUM);

	for (size_t i = 0; i < NUM; i++)
	{
		vv[i].resize(i + 1);
		vv[i][0] = vv[i][vv[i].size() - 1] = 1;
		//vv[i].front() = vv[i].back() = 1;//跟上面一行表达意思一样
	}

	for (size_t i = 0; i < vv.size(); i++)
	{
		for (size_t j = 0; j < vv[i].size(); j++)
		{
			if (vv[i][j] == 0)
			{
				vv[i][j] = vv[i - 1][j] + vv[i - 1][j - 1];
			}
		}
	}

	for (size_t i = 0; i < vv.size(); i++)
	{
		for (size_t j = 0; j < vv[i].size(); j++)
		{
			cout << vv[i][j] << " ";
		}
		cout << endl;
	}

	return 0;
}