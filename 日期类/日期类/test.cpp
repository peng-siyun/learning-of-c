#include "data.h"

void test1()
{
	//Data d1;
	//Data d2(2022, 9, 17);
	//d1.Print();
	//d2.Print();

	Data d3(2022, 13, 25);
	//d3.Print();

	Data d4(2022, 2, 29);
	//d4.Print();

	Data d5(2020, 2, 29);
	//d5.Print();

	Data d6(2000, 2, 29);
	//d6.Print();
}

void test2()
{
	Data d1(2022, 9, 18);
	d1.Print();

	d1 += 100;
	d1.Print();

	Data ret1 = d1++;
	d1.operator++(0);//括号中任意值都行，但必须是整型，是为了占位
	d1.operator++(1);

	Data ret2 = ++d1;
	d1.operator++();
}

void test3()
{
	Data d1(2022, 1, 18);

	//Data ret1 = d1 - 10;
	//ret1.Print();
	//d1.Print();

	//Data ret2 = d1 - 40;
	//ret2.Print();

	//Data ret3 = d1 - -40; //--40等于+40
	//ret3.Print();

	//Data ret1 = d1 + 10;
	//ret1.Print();
	//d1.Print();

	//Data ret2 = d1 + 40;
	//ret2.Print();

	Data ret3 = d1 + -40; //--40等于+40
	ret3.Print();
}

void test4()
{
	Data d1(2022, 9, 18);

	Data ret = d1--;
	d1.Print();
	ret.Print();

	//Data ret1 = --d1;
	//d1.Print();
	//ret1.Print();
}

void test5()
{
	Data today(2022, 9, 18);

	Data offerday(2023, 3, 1);

	cout << (offerday - today) << endl;
	cout << (today - offerday) << endl;

	today.PrintWeekDay();

	Data(2022, 9, 19).PrintWeekDay(); //匿名对象，生命周期只在这一行
}

void test6()
{
	Data d1;
	Data d2;

	//d1.operator<<(cout); //等同于 d1 << cout
	//d1 << cout

	cout << d1;
	operator<< (cout, d1);
	cout << d1 << d2;


	//cin >> d2;
	cin >> d1 >> d2;
}
int main()
{
	
	//test2();
	////test3();
	//test5();
	test6();

	return 0;
}