#include "data.h"

int Data::GetMonthDay(int year, int month)
{
	static int monthDayArray[13] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };
	int day = monthDayArray[month];

	if (month == 2 && ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)))//判断闰年
	{
		day += 1;
	}
	return day;
}

Data::Data(int year, int month, int day)
{
	_year = year;
	_month = month;
	_day = day;

	if(!(_year >=0 
		&& (_month > 0 && _month<13)
		&& (day > 0 && day <= GetMonthDay(year, month))))
	{
		cout<<"非法日期->";
		Print();//等价于this->Print()
	}
}

void Data::Print() const //this指针类型为const Data& const this
{
	cout << _year << "-" << _month << "-" << _day << endl;
}

bool Data::operator> (const Data& d2)const  //d > d2
{
	if (_year > d2._year)
	{
		return true;
	}
	else if (_year == d2._year && _month > d2._month)
	{
		return true;
	}
	else if (_year == d2._year  && _month > _month  && _day > _day)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool Data::operator== (const Data& d2)const //d == d2
{
	return _year == d2._year && _month == d2._month  &&_day == d2._day;
}

bool Data::operator>= (const Data& d2)const //d >= d2
{
	return *this > d2 || *this == d2;
}

bool Data::operator< (const Data& d2)const //d < d2
{
	return !(*this >= d2);
}

bool Data::operator<= (const Data& d2) const//d <= d2
{
	return !(*this > d2);
}

bool Data::operator!= (const Data& d2)const //d != d2
{
	return !(*this == d2);
}

Data& Data::operator+=(int day) //d1 += 100
{
	if (day < 0)
	{
		return *this -= -day;
	}
	_day += day;
	while (_day > GetMonthDay(_year, _month))
	{
		_day -= GetMonthDay(_year, _month);
		++_month;

		if (_month == 13)
		{
			_month = 1;
			++_year;
		}
	}

	return *this;
}

Data Data::operator+(int day) //d1 + 100,注意不改变d1的值
{
	Data ret(*this); //先复制拷贝一份
	ret += day;//ret.operator+=(day)
	return ret;
}

Data& Data::operator-=(int day)
{
	if (day < 0)
	{
		return *this += -day;
	}
	_day -= day;
	while (_day <= 0)
	{
		--_month;

		if (_month == 0)
		{
			--_year;
			_month = 12;
		}

		_day += GetMonthDay(_year, _month);
	}

	return *this;
}

Data Data::operator-(int day)
{
	Data ret(*this); //先复制拷贝一份
	ret -= day;//ret.operator-=(day)
	return ret;
}

Data& Data::operator++() //前置++,推荐自定义类型用前置++，避免多次调用拷贝
{
	*this += 1;
	return *this;
}

Data Data::operator++(int)  //后置++
{
	Data ret(*this);//先复制拷贝一份
	*this += 1;

	return ret;  //后置++比前置++多两次拷贝构造
}

Data& Data::operator--()
{
	*this -= 1;
	return *this;
}
Data Data::operator--(int)
{
	Data ret(*this);
	*this -= 1;

	return ret;
}

//offerday - today
int Data::operator-(const Data& d)
{
	Data max = *this;
	Data min = d;
	int flag = 1;

	if (*this < d)
	{
		max = d;
		min = *this;
		flag = -1;
	}

	int count = 0;
	while (min != max)
	{
		++count;
		++min;
	}

	return count * flag;
}

void Data::PrintWeekDay()
{
	const char* arr[] = {"星期一", "星期二", "星期三", "星期四", "星期五", "星期六", "星期日", };
	//指针数组，指针数组比较适合用来指向若干个字符串

	//Data start(1900, 1, 1);
	//int count = *this - start;
	int count = *this - Data(1900, 1, 1); //匿名对象

	//cout << "星期" << count % 7 + 1 << endl;
	cout << arr[count % 7] << endl;
}

void Data::operator<<(ostream& out) //等同于 d1 << cout，第一个位置已被占领了，是this
{
	out << _year << "/" << _month << "/" << _day << endl;
}

ostream& operator<<(ostream& out, const Data& d)
{
	out << d._year << "/" << d._month << "/" << d._day << endl;
	return out;
}

istream& operator>>(istream& in, Data& d)
{
	in >> d._year>> d._month >> d._day;
	return in;
}

