#pragma once
#include <iostream>
using namespace std;

class Data
{
	//友元函数
	friend ostream& operator<<(ostream& out, const Data& d);
	friend istream& operator>>(istream& in, Data& d);


public:

	void operator<<(ostream& out);
	//运算符重载里面，如果是双操作数的操作符重载，第一个参数是左操作数，第二个参数是右操作数
	//等同于 d1 << cout，第一个位置已被占领了，是this
	//所以不能实现成成员函数了，所以可以搞成全局的,但是全局就不能类中私有成员变量，可以用友元
	//void operator>>(ostream& out);


	Data(int year = 0, int month = 1, int day = 1);
	void Print() const;
	int GetMonthDay(int year, int month);  //计算这一年是不是闰年，并且计算该年二月多少天

	//d1.operator>(d2); //调用成员函数,隐藏了this指针
	//所有此类比较，都可以先实现‘>’"=="，其他都可以复用
	bool operator> (const Data& d2)const; //bool operator> (Data& const this, const Data& d2)
	bool operator< (const Data& d2)const; //可以考虑复用‘>’'=='
	bool operator<= (const Data& d2)const;
	bool operator>= (const Data& d2)const;
	bool operator== (const Data& d2)const;
	bool operator!= (const Data& d2)const;

	Data& operator+=(int day);
	Data operator+(int day);

	Data& operator-=(int day);
	Data operator-(int day);

	//++d1;    d1.operator++(&d1)
	Data& operator++();
	//d1++;后置++为了跟前置++进行区分，增加一下参数定位,跟前置++构成了函数重载
	Data operator++(int);//d1.operator++(&d1,0)

	Data& operator--();
	Data operator--(int);

	int operator-(const Data& d);// 计算两个日期之间差多少天

	void PrintWeekDay();


private:
	int _year;
	int _month;
	int _day;
};

