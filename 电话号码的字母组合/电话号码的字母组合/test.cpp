#include <iostream>
#include <vector>
#include <string>
using namespace std;

string arr[10] = { "", "", "abc","def","ghi","jkl","mno","pqrs","tvu","wxyz" };

void _letterCombinations(const string& digits, size_t i, string combinStr, vector<string>& strV)
{
	if (i == digits.size())
	{
		strV.push_back(combinStr);
		return;
	}

	string str = arr[digits[i] - '0'];
	for (size_t j = 0; j < str.size(); j++)
	{
		_letterCombinations(digits, i + 1, combinStr + str[j], strV);
	}
}
int main()
{
	string digits("234");
	string combinStr;
	vector<string> strV;
	_letterCombinations(digits, 0, combinStr, strV);

	int count = 0;
	for (size_t i = 0; i < strV.size(); i++)
	{
		for (size_t j = 0; j < strV[i].size(); j++)
		{
			cout << strV[i][j];
		}
		cout << endl;
		count++;
	}
	cout << count << endl;
	return 0;
}