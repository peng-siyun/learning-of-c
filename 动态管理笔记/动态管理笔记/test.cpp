#include <iostream>
using namespace std;

class Listnode
{
public:
	Listnode(int data)
		:_next(nullptr)
		,_prev(nullptr)
		,_data(data)
	{}
	//因他没有在堆中开辟空间，所以不用写显示析构函数，默认析构函数就够用了
	Listnode* _next;
	Listnode* _prev;
	int _data;
};

class List
{
public:
	List ()
	{
		_head = new Listnode(10);
		_head->_next = _head;
		_head->_prev = _head;
	}

	void pushBack(int data)
	{
		//new = operator new + 调用构造函数初始化
		//operator new底层是malloc，是malloc的封装，因malloc失败会返回空指针，而operator new失败是抛异常
		//delete = 先调用析构函数清理空间 + operator delete
		//operator delete底层是free，是free的封装
		Listnode* newnode = new Listnode(data);
		Listnode* tail = _head->_prev;
		tail->_next = newnode;
		newnode->_prev = tail;
		newnode->_next = _head;
		_head->_prev = newnode;
	}

	~List()
	{
		Listnode* cur = _head->_next;
		while (cur != _head)
		{
			Listnode* next = cur->_next;
			delete cur;
			cur = next;
		}

		delete _head;
		_head = nullptr;
	}

private:
	Listnode* _head;

};

class Stack
{
public:
	Stack(int capacity = 4)
		:_top(0)
		, _capacity(capacity)
	{
		_a = new int[capacity];
	}

	~Stack()
	{
		delete[] _a;
		_a = nullptr;
		_top = 0;
		_capacity = 0;
	}
private:
	int* _a;
	int _top;
	int _capacity;
};
int main()
{
	List l;
	l.pushBack(1);
	l.pushBack(2);

	Listnode* p = (Listnode*)malloc(sizeof(Listnode));
	new(p)Listnode(5);//定位new，replace new，手动调用构造函数   //使用方法：new（地址）自定义类

	Listnode* p2 = new Listnode(7);
	delete p2;

	//等价于如下
	Listnode* p3 = (Listnode*)operator new (sizeof(Listnode));//operator new底层就是malloc，使用方式一样
	new(p3)Listnode(8);

	p3->~Listnode();//可以显示调用析构函数
	operator delete(p3);//匹配起来，析构完释放空间
	return 0;
}