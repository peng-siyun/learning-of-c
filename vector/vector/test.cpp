#include <iostream>
#include <vector>
using namespace std;

void test1()
{
	vector<int> v1;
	vector<int> v2(10, 8);
	vector<int> v3(++v2.begin(), --v2.end());
	vector<int> v4(v3);

	string s("hello world");
	vector<char> v5(s.begin(), s.end());
}
void test2()
{
	vector<int> v;
	v.push_back(1); //尾插
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);
	v.pop_back();//尾删
	v.push_back(4);

	//遍历,因为返回是引用，所以也可以修改，因为底层是数组，所以支持【】
	for (size_t i = 0; i < v.size(); i++)
	{
		v[i] += 1;
		cout << v[i] << " ";
	}
	cout << endl;

	//迭代器
	vector<int>::iterator it = v.begin();
	while (it != v.end())
	{
		*it -= 1;
		cout << *it << " ";
		++it;
	}
	cout << endl;

	//范围for
	for (auto e : v)
	{
		cout << e << " ";
	}
	cout << endl;
}

void test3()
{
	vector<int> v;
	v.push_back(1); //尾插
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);
	v.pop_back();//尾删
	v.push_back(4);

	cout << v.max_size() << endl;

	//v.reserve(100);//扩容
	//v.resize(100, 5);//扩容+初始化或删除数据

	//v.assign(10, 5);//覆盖掉之前的数据，以前1到4，现在10个5

	vector<int>::iterator ret = find(v.begin(), v.end(), 3);//算法库中
	//cout << *ret << endl;

	//v.insert(ret, 30);
	v.insert(v.begin(), -1);//插入

	v.erase(ret);//删除
	v.clear();
}
int main()
{
	test3();

	return 0;
}