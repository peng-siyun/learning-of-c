#include <iostream>
using namespace std;

class A
{
public:
	A(int a = 0)
	{
		cout << "A(int a = 0)" << endl;
		_a = a;
	}

	A(const A& aa)
	{
		cout << "A(const A& aa )" << endl;
		_a = aa._a;
	}

	A& operator=(const A& aa)
	{
		cout << "operator=(const A& aa)" << endl;
		_a = aa._a;

		return *this;
	}
private:
	int _a;
};

class Date
{
public:
	Date(int year, int month, int day, const A& aa) //普通构造函数
		: _aa(aa)
	{
		_year = year;
		_month = month;
		_day = day;
	}

	// 每个成员变量在初始化列表中只出现一次（初始化只能初始化一次）
	//类中包含以下成员，必须在初始化列表位置进行初始化
	//①引用成员变量②const成员变量③自定义类型成员变量（没有默认构造函数，类似A）
	//初始化列表->每个对象成员变量定义的地方
	//Date(int year, int month, int day, int i) 
	//	: _year(year)
	//	, _month(month)
	//	, _day(day)
	//	, _N(10)
	//	, _ref(i)
	//	, _aa(4)
	//{}

	//成员变量在类中声明次序就是其在初始化列表中的初始化顺序，与其在初始化列表中的先后次序无关
private:
	int _year; //声明
	int _month;
	int _day;

	//const int _N;
	//int& _ref; //引用必须在定义的时候初始化
	A _aa;//没有默认构造函数的自定义类型成员变量必须在定义的时候初始化
};
//int main()
//{
//	A a(10);
//	
//	//int i = 1;
//	//const int j; //const常量必须在定义的时候初始化 
//
//	Date d1(2022, 9, 18, a);//对象实例化
//
//	return 0;
//}
class Data
{
public:
	//explicit Data(int year)// explicit关键字就可一阻止此类隐式转换
	//	:_year(year)
	//{}

	Data(int year)
		:_year(year)
	{}

	//int getCount()
	//{
	//	return _sCount;//因为私有成员变量外界无法访问，可以调用此函数访问
	//}

	//没有this指针，只能访问静态成员变量，生命周期在整个程序运行期间
	static int getCount()
	{
		return _sCount;
	}
private:
	int _year = 0; //这里不叫初始化，叫给缺省值，是在初始化列表中初始化，如果在初始化列表你为初始化，就用该缺省值

	//静态成员变量属于整个类，所有对象；生命周期在整个程序运行期间
	//类的成员函数中随便访问
	static int _sCount;//声明，静态不能给缺省值，只能类外初始化
};
int Data::_sCount = 0;//类外初始化,仅限于静态成员变量

int main()
{
	Data d1(2022);
	//Data d2 = 2022; //隐式类型转换
	//本来用2022构造一个临时对象Data（2022），再用这个对象拷贝构造d2
	//但是C++编译器在连续的一个过程中，多个构造会被优化，合二为一，所以这里被优化为直接就是一个构造了
	//虽然他们两都是直接构造，但是过程是不一样，前者是一次构造，后者是一次构造一次拷贝

	//隐式类型转换--意义相似类型
	double i = 1.1;
	int j = i;
	const int& b = i;//临时变量

	//强制类型转换--无关类型
	int* p = &j;
	int q = (int)p;

	cout << d1.getCount() << endl;
	cout << Data::getCount() << endl;// 使用对象去调

	return 0;
}