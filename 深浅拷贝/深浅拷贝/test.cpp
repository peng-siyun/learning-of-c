#include <iostream>
using namespace std;

namespace psy
{
	class string
	{
	public:
		string(const char* str)
			:_str(new char[strlen(str) + 1])
		{
			strcpy(_str, str); //两个字符串拷贝用strcpy
		}
//
//		string(const string& s)
//			:_str(new char[strlen(s._str) + 1])
//		{
//			strcpy(_str, s._str);
//		}
//
//		//s1 = s3
//		string& operator=(const string& s)
//		{
//			if (this != &s)//考虑自己给自己赋值
//			{
//				//delete[] _str;
//				//_str = new char[strlen(s._str) + 1];
//				//strcpy(_str, s._str);
//				//return *this;
//
//				char* tmp = new char[strlen(s._str) + 1];//避免抛异常直接跳过
//				strcpy(tmp, s._str);
//				delete[] _str;
//				_str = tmp;
//			}
//			return *this;
//
//		}
//
//		~string()
//		{
//			delete[] _str;
//			_str = nullptr;
//		}

//实现一个简洁的string拷贝构造
	//s2(s1)
	public:
		string(const string& s)
			:_str(nullptr)
		{
			string tmp(s._str);
			swap(_str, tmp._str);
		}
	private:
		char* _str;
	};
}

namespace psy
{
	class string
	{
	public:

		//迭代器
		typedef char* iterator;

		iterator begin()
		{
			return _str;
		}
		
		iterator end()
		{
			return _str + _size;
		}
		//string()
		//	:_str(new char[1])
		//	,_size(0)
		//	,_capacity(0)
		//{
		//	_str[0] = '\0';
		//}
		string(const char* str = "")//给空字符串，默认还有一个\0;这个缺省值跟上面代码功能一样
			:_size(strlen(str))
			,_capacity(_size)//有效字符，不算\0
		{
			_str = new char[_capacity + 1];
			strcpy(_str, str); 
		}

		string(const string& s)
			:_size(s._size)
			,_capacity(s._capacity)
		{
			_str = new char[_capacity + 1];
			strcpy(_str, s._str);
		}

		//s1 = s3
		string& operator=(const string& s)
		{
			if (this != &s)//考虑自己给自己赋值
			{
				char* tmp = new char[s._capacity + 1];//避免抛异常直接跳过
				strcpy(tmp, s._str);
				delete[] _str;
				_str = tmp;
				_size = 0;
				_capacity = 0;
			}
			return *this;

		}

		~string()
		{
			delete[] _str;
			_str = nullptr;
			_size = 0;
			_capacity = 0;
		}

		const char* c_str() const
		{
			return _str;//返回字符串的指针
		}

		size_t size() const
		{
			return _size;
		}

		char& operator[](size_t pos)
		{
			return _str[pos];
		}
	private:
		char* _str;
		size_t _size;
		size_t _capacity;
	};

	void test1()
	{
		string s1("hello");

		for (size_t i = 0; i < s1.size(); i++)
		{
			cout << s1[i] << " ";
		}
		cout << endl;
	}


}




int main()
{
	return 0;
}