#include <iostream>
using namespace std;
#include "Data.h"


int main()
{
	Date d;
	Date d2(2022, 1, 1);
	
	//Date d3(d2);
	//d3.Print();
	//d = d3;
	//d.Print();

	//d2 += 40;
	d2 -= 10;
	d2++;
	--d2;
	d2.Print();

	cout << (d < d2) << endl;
	cout << d - d2 << endl;

	return 0;
}